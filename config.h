static int topbar = 1;                          // Position: up = 1, down = 0

// Set Font
static const char *fonts[] = {
	"Ibm Plex Mono Text:pixelsize=15",
	"JoyPixels Regular:pixelsize=15",
};

static const char *prompt = "";          // Set Prompt

static const char *colors[SchemeLast][2] = {
	                      /*     fg         bg     */
	[SchemeNorm]          = { "#c5c8c6", "#1d1f21" },
	[SchemeSel]           = { "#1d1f21", "#b1b4b2" },
	[SchemeOut]           = { "#000000", "#00FFFF" },
    [SchemeSelHighlight]  = { "#f0c674", "#b1b4b2" },
    [SchemeNormHighlight] = { "#f0c674", "#1d1f21" },
    [SchemeMid]           = { "#ffffff", "#1d1f21" },
};

// Set horizontal line numbers if 0 all in one line
static unsigned int lines = 40;
static unsigned int lineheight = 0;              // Set line height, option -h
static unsigned int border_width = 3;           // Border size
static unsigned int columns = 4;               // Number of grids, option -g

// Centered on = 1 or option -c, off = 0
static int centered  = 1;
static int min_width = 1200;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
